package animal;

public class Animal {

    // private, protected, public
    // default is protected
    private String name ;
    private String species = "Animal";
    private int age;

    public static boolean visitable;

    public Animal(String nameParameter, String species, int age) {
        this.name = nameParameter;
        this.species = species;
        this.age = age;
    }

    public Animal(String nameParameter, String species, int age, boolean visitable) {
        this.name = nameParameter;
        this.species = species;
        this.age = age;
        this.visitable = visitable;
    }

    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Name: " + name + ", Species: " + species + ", Age: " + age + ", Vistable: " + visitable;
    }

}

